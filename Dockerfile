FROM php:8.0-fpm-alpine

LABEL maintainer="Sergey Kuzmichev <mail@kuzmichevs.ru>"

RUN apk update && \
    apk upgrade --available && \ 
    sync

COPY . /var/www/html

WORKDIR /var/www/html
